'''
jc_colorOverride.py

Author: John Cordeiro
Date: 1/20/16
Email: john@johncordeiro.com
URL: http://johncordeiro.com

Description:
Enables and disables the overrideEnabled setting for the selected objects.
Changes the display color of the selected objects.

Usage:
import jc_colorOverride
DisplayColorOverrideUI.display()

'''

import maya.cmds as cmds
import maya.OpenMaya as om


class DisplayColorOverride(object):
    '''
    Class for modifying the drawing override color of selected objects.
    No UI code is contained in this class.
    
    @param object, str, name of object to apply override to.
    '''
    MAX_OVERRIDE_COLORS = 32
    
    @classmethod
    def override_color(cls, color_index):
        '''
        Classmethod to set override color to color_index.
        Checks if color_index is in range and selected object has a shape node.
        
        @param color_index, int, value of index color (0-31)
        @return Bool, True if color is set, False if fails.
        '''
        if (color_index >= cls.MAX_OVERRIDE_COLORS or color_index < 0):
            om.MGlobal.displayError('Color index out-of-range (must be between 0-31)')
            return False
            
        shapes = cls.shape_nodes()
        if not shapes:
            om.MGlobal.displayError('No shape nodes selected')
            return False
            
        for shape in shapes:
            cmds.setAttr('{0}.overrideEnabled'.format(shape), True)
            cmds.setAttr('{0}.overrideColor'.format(shape), color_index)
            
        return True
    
    
    @classmethod
    def use_defaults(cls):
        '''
        Classmethod to disable override color and set to default.
        @return Bool, True if succesfull, False if fail.
        '''
        shapes = cls.shape_nodes()
        if not shapes:
            om.MGlobal.displayError('No shape nodes selected')
            return False
            
        for shape in shapes:
            cmds.setAttr('{0}.overrideEnabled'.format(shape), False)
            
        return True
            
            
    @classmethod
    def shape_nodes(cls):
        '''
        Classmethod to get shapes of selected objects.
        
        @return shapes, list[str], list of shapes from selected nodes.
        '''
        selection = cmds.ls(sl=1)
        if not selection:
            return None
            
        shapes = []
        for node in selection:
            shapes.extend(cmds.listRelatives(node, shapes=1))
            
        return shapes
    

class DisplayColorOverrideUI(object):
    '''
    Creates the GUI that can be used to display the drawing override color palette
    and change the override color of the selected objects.
    
    @param object, str, name of object to apply overrides to.
    '''
    WINDOW_NAME = 'JC_DisplayColorOverride'
    
    COLOR_PALETTE_CELL_WIDTH = 22
    
    FORM_OFFSET = 5
    
    color_palette = None
    
    @classmethod
    def display(cls):
        '''
        Classmethod to display UI
        '''
        cls.delete()
        
        main_window = cmds.window(cls.WINDOW_NAME, title='JC_DisplayColorOverride', rtf=1, sizeable=False)
        main_layout = cmds.formLayout(parent=main_window)
        
        rows = 2
        columns = DisplayColorOverride.MAX_OVERRIDE_COLORS / rows
        width = columns * cls.COLOR_PALETTE_CELL_WIDTH
        height = rows * cls.COLOR_PALETTE_CELL_WIDTH
        cls.color_palette = cmds.palettePort(dimensions = (columns, rows),
                                             transparent = 0, 
                                             width = width,
                                             height = height,
                                             topDown = True,
                                             colorEditable = False,
                                             parent = main_layout);
        
        for index in range(1, DisplayColorOverride.MAX_OVERRIDE_COLORS):
            color_component = cmds.colorIndex(index, q=True)
            cmds.palettePort(cls.color_palette, edit=True,
                             rgbValue=(index, color_component[0], color_component[1], color_component[2]))
            
            cmds.palettePort(cls.color_palette, e=True, rgbValue=(0, 0.6, 0.6, 0.6), r=True)
            
            # Create override and default buttons
            override_button = cmds.button(l='Override', 
                                          command = 'DisplayColorOverrideUI.override()',
                                          parent = main_layout)
            
            default_button = cmds.button(l='Default',
                                         command = 'DisplayColorOverrideUI.default()',
                                         parent = main_layout)
            
            # Layout out the Color Palette
            cmds.formLayout(main_layout, e=True,
                            attachForm=(cls.color_palette, 'top', cls.FORM_OFFSET))
            
            cmds.formLayout(main_layout, e=True,
                            attachForm=(cls.color_palette, 'right', cls.FORM_OFFSET))
            
            cmds.formLayout(main_layout, e=True,
                            attachForm=(cls.color_palette, 'left', cls.FORM_OFFSET))
            
            # Layout the override and default buttons
            cmds.formLayout(main_layout, e=True,
                            attachControl = (override_button, 'top', cls.FORM_OFFSET, cls.color_palette))
            cmds.formLayout(main_layout, e=True,
                            attachForm = (override_button, 'left', cls.FORM_OFFSET))
            cmds.formLayout(main_layout, e=True,
                            attachPosition = (override_button, 'right', 5, 50))
            
            cmds.formLayout(main_layout, e=True,
                            attachOppositeControl = (default_button, 'top', 0, override_button))
            cmds.formLayout(main_layout, e=True,
                            attachControl = (default_button, 'left', 0, override_button))
            cmds.formLayout(main_layout, e=True,
                            attachForm = (default_button, 'right', cls.FORM_OFFSET))
            
                            
            cmds.showWindow(main_window)                
        
    @classmethod
    def delete(cls):
        if cmds.window(cls.WINDOW_NAME, exists=True):
            cmds.deleteUI(cls.WINDOW_NAME, window=True)
        
    @classmethod
    def override(cls):
        color_index = cmds.palettePort(cls.color_palette, q=True, setCurCell=True)
        DisplayColorOverride.override_color(color_index)
    
    @classmethod    
    def default(cls):
        DisplayColorOverride.use_defaults()
        
    
if __name__ == '__main__':
    DisplayColorOverrideUI.display()
    