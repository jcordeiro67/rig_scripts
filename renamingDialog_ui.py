# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/jcordeiro/Documents/QT_Projects/renamingDialog_ui.ui'
#
# Created: Thu Jan 28 14:36:39 2016
#      by: pyside-uic 0.2.14 running on PySide 1.2.0
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_renamingDialog(object):
    def setupUi(self, renamingDialog):
        renamingDialog.setObjectName("renamingDialog")
        renamingDialog.setWindowModality(QtCore.Qt.NonModal)
        renamingDialog.resize(280, 355)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(renamingDialog.sizePolicy().hasHeightForWidth())
        renamingDialog.setSizePolicy(sizePolicy)
        self.verticalLayoutWidget_2 = QtGui.QWidget(renamingDialog)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(8, 8, 265, 337))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")
        self.main_layout = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.main_layout.setContentsMargins(2, 0, 2, 0)
        self.main_layout.setObjectName("main_layout")
        self.selectionList_layout = QtGui.QVBoxLayout()
        self.selectionList_layout.setContentsMargins(0, 0, 0, 0)
        self.selectionList_layout.setObjectName("selectionList_layout")
        self.selectionList = QtGui.QListWidget(self.verticalLayoutWidget_2)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Ignored)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.selectionList.sizePolicy().hasHeightForWidth())
        self.selectionList.setSizePolicy(sizePolicy)
        self.selectionList.setResizeMode(QtGui.QListView.Adjust)
        self.selectionList.setSpacing(2)
        self.selectionList.setUniformItemSizes(False)
        self.selectionList.setObjectName("selectionList")
        self.selectionList_layout.addWidget(self.selectionList)
        self.button_layout = QtGui.QHBoxLayout()
        self.button_layout.setContentsMargins(2, 0, 2, 0)
        self.button_layout.setObjectName("button_layout")
        self.refresh_button = QtGui.QPushButton(self.verticalLayoutWidget_2)
        self.refresh_button.setObjectName("refresh_button")
        self.button_layout.addWidget(self.refresh_button)
        self.cancel_button = QtGui.QPushButton(self.verticalLayoutWidget_2)
        self.cancel_button.setObjectName("cancel_button")
        self.button_layout.addWidget(self.cancel_button)
        self.selectionList_layout.addLayout(self.button_layout)
        self.main_layout.addLayout(self.selectionList_layout)

        self.retranslateUi(renamingDialog)
        QtCore.QMetaObject.connectSlotsByName(renamingDialog)

    def retranslateUi(self, renamingDialog):
        renamingDialog.setWindowTitle(QtGui.QApplication.translate("renamingDialog", "PyQt - Renaming Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.refresh_button.setText(QtGui.QApplication.translate("renamingDialog", "Refresh", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_button.setText(QtGui.QApplication.translate("renamingDialog", "Cancel", None, QtGui.QApplication.UnicodeUTF8))

