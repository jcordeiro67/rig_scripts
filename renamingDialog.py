from maya import OpenMayaUI as omui
from PySide import QtCore
from PySide import QtGui
from shiboken import wrapInstance
from maya.app.general.mayaMixin import MayaQWidgetDockableMixin

import pymel.core as pm

import renamingDialog_ui
reload(renamingDialog_ui)

def maya_main_window():
    main_window_ptr = omui.MQtUtil.mainWindow()
    return wrapInstance(long(main_window_ptr), QtGui.QWidget)
    
class Ui_RenamingDialog(QtGui.QDialog, renamingDialog_ui.Ui_renamingDialog):
    def __init__(self, parent=maya_main_window()):
        super(Ui_RenamingDialog, self).__init__(parent)
        self.setupUi(self)
        
        self.setWindowFlags(QtCore.Qt.Tool)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        
        self.create_connections()
    
    def create_connections(self):
        self.refresh_button.clicked.connect(self.refresh_ui)
        self.cancel_button.clicked.connect(self.cancel_ui)
        
    def update_selection_list(self):
        sel = pm.ls(sl=1)
        # set the s variable to a string
        for s in sel:
            str(s)
            print s
            listItem = QtGui.QListWidgetItem(s)
            listItem.setFlags(listItem.flags() | QtCore.Qt.ItemIsEditable)
            # this line is causing an error
            # File "<maya console>", line 40, in update_selection_list
            # AttributeError: 'PySide.QtGui.QListView' object has no attribute 'addItem'
            self.selectionList.addItem(listItem)
        
        
    #------------------------------------------------------------------------
    # SLOTS
    #------------------------------------------------------------------------
    
    def refresh_ui(self):
        self.update_selection_list()
        
    def cancel_ui(self):
        self.close()
    
if __name__ == '__main__':
    try:
        UI.close()
    except:
        pass
    
    UI=Ui_RenamingDialog()
    UI.show()
    
 
