'''
    Script:     jc_splitSelJoint.py
    Version:    1.0
    Author:     John Cordeiro
    Website:    http://johncordeiro.com
    
    Description:    Splits the selected joint into the number of given segments

    How to use:   import jc_splitSelJoint
                  reload(jc_splitSelJoint)
                  jc_splitSelJoint.splitSelJoint(numSegments)  numSegments must be > 1
    
'''

import pymel.core as pm

def splitSelJoint(numSegments):
    '''
    Split the selected joint into the given number of segments and renames the new joints.
    
    @param numSegments: int, the number of segemnts to split the selected joint into
    @return: None
    
    '''
    if numSegments < 2:
        pm.mel.error('The number of segments has to be greater then 1')
        
    # variables
    
    joints = pm.ls(sl=True, type='joint')
    
    for joint in joints:
        
        # check is child exists
        child = getChildJoint(joint)
        if child == []:
            pm.mel.error('Joint: ' + joint + ' has no child joints.\n')
            
        else:
            # get joint radius
            jointRadius = joint.getRadius()
            # get the axis attr
            axis = getJointAxis(child)
            attr = ('t' + axis)
            # get joint rotation order
            rotOrderindex = pm.getAttr(joint+'.rotateOrder')
            # get the value of the childs translation
            childT = pm.getAttr(child + '.' + attr)
            # divide childs translation by numSegments
            space = childT / numSegments
            
            # create a series of locators along the joint base off the number of segments
            # and divide them evenly along the joint.
            locators = []
            for x in range(0, (numSegments -1)):
                tmp = pm.spaceLocator()
                locators.append(tmp)
                pm.parent(locators[x], joint)
                locators[x].translate.set(0,0,0)
                pm.setAttr((locators[x]+'.'+attr), ( space * (x+1)) )
                
                
            # for each segment insert a joint and move it to the locator.
            prevJoint = joint
            
            for x in range(0, len(locators)):
                # insert new joint
                newJoint = pm.insertJoint(prevJoint)
                # get locator transformation
                pos = pm.xform(locators[x], q=1, t=1, ws=1)
                # move newJoint to locator
                pm.move(pos[0], pos[1], pos[2], newJoint+'.scalePivot', newJoint+'.rotatePivot', a=1, ws=1)
                # rename the newJoint and number the segment
                newJoint = pm.rename(newJoint, "{0}{1}{2}{3}".format(joint, '_seg_', x, '_joint'))
                # set the joints radius and rotation order
                pm.setAttr(newJoint + '.radius', jointRadius)
                pm.setAttr(newJoint + '.rotateOrder', rotOrderindex)
                
                prevJoint = newJoint
                
            pm.delete(locators)
            
            
def getJointAxis(child):
    '''
    procedure to find the axis running down the joint chain.
    @param child: str, name of child joint.
    @return axis: str, axis running down joint chain (x, y, z)
    '''
    axis = ''
    t = child.translate.get()
    tol = 0.0001
    
    for x in range(0,2+1):
        
        if ((t[x] > tol) or (t[x] < (-1 * tol))):
            
            if x == 0:
                axis = 'x'
            if x == 1:
                axis = 'y'
            if x == 2:
                axis = 'z'

                    
    if axis == '':
        pm.mel.error('The child joint is too close to the parent joint, can not determine the axis to segment along')
        
    return axis


def getChildJoint(joint):
    '''
    procedure to find the child joint of a given joint.
    @param joint: str, name of joint to find child of
    @return child: str, name of child joint
    '''
    child = pm.listRelatives(joint, f=1, c=1, type='joint')[0]
    return child