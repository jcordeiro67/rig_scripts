import pymel.core as pm

'''
pymel ikfk switch proof of concept

John Cordeiro - johncordeiro.com - john@johncordeiro.com

Creates a very basic IKFK switch using pymel

Create a 3 joint chain, select the root joint of the newly created chain and execute
After running you should have 3 joint chains baseJoints, fkJoints, ikJoints
and a nurbs circle(ikfk Switch)

select the root ik joint and rotate it, then select the nurbs circle and slide ikfkSwitch
to see the joints switching from one driver chain to the other driver chain.

'''

baseJoints = pm.ls(sl=1, type='joint', dag=1)

fkJoints = pm.duplicate(baseJoints, rc=1)
for joint in fkJoints:
    newName = joint+'_fk'
    joint.rename(newName)
    
ikJoints = pm.duplicate(baseJoints, rc=1)
for joint in ikJoints:
    newName = joint+'_ik'
    joint.rename(newName)
    
ikFkSwitch, shape=pm.circle(n='ikfkSwitch', nr=[1,0,0], c=[0,0,0], r=1, sw=360, d=3, s=8)
pm.addAttr(ikFkSwitch, ln='ikfkSwitch', at='float', min=0, max=10, dv=0, k=1)

ikfkSwitchBuffer = pm.shadingNode('multiplyDivide', au=1, n='left_leg_ikfk_buffer_mda')
ikfkSwitchBuffer.setAttr('operation', 1)
ikfkSwitchBuffer.setAttr('input2X', .1)

ikFkSwitch.ikfkSwitch >> ikfkSwitchBuffer.input1X

ikfkBlendColor = pm.shadingNode('blendColors', au=1, n=baseJoints[0]+'_ikFkSwitch_blendNode')
fkJoints[0].rotate >> ikfkBlendColor.color2
ikJoints[0].rotate >> ikfkBlendColor.color1

ikfkSwitchBuffer.outputX >> ikfkBlendColor.blender
ikfkBlendColor.output >> baseJoints[0].rotate

